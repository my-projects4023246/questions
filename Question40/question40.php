<?php

    echo "<html>
            <head>
                <link rel='stylesheet' href='bootstrap/bootstap.css'>
                <link rel='stylesheet' href='style.css'>
                <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'> 
            </head>
           
        </html>";

    $i = 1;

        
    echo "<div class='alert alert-info' role='alert' id='question'> 
        <h4 class='alert-heading'>Question 40</h4>
        <p>Write a program to print even nos. out of first 100 numbers.</p>
        <hr>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='card text-center'>
                        <div class='card-body'>
                            <h5 class='card-title'>Even Numbers</h5>";
                            
                            while ($i <= 100) {
                                if ($i % 2 == 0) {
                                    echo "$i,   ";

                                }
                                $i++;
                            }

            echo "      </div>
                    </div>
                </div>
            </div>
        </div>";

?>

